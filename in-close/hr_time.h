#ifndef HR_TIME_H
#define HR_TIME_H

#ifdef _WIN32
#include <windows.h>

typedef struct {
    LARGE_INTEGER start;
    LARGE_INTEGER stop;
} stopWatch;

class CStopWatch {

private:
	stopWatch timer;
	LARGE_INTEGER frequency;
	double LIToSecs( LARGE_INTEGER & L);
public:
	CStopWatch();
	void startTimer( );
	void stopTimer( );
	double getElapsedTime();
};

#endif

#if __APPLE__

#include <assert.h>
#include <CoreServices/CoreServices.h>
#include <mach/mach.h>
#include <mach/mach_time.h>
#include <unistd.h>

class CStopWatch {

 private:
  uint64_t start;
  uint64_t end;
  uint64_t elapsed;
  uint64_t elapsedNano;
  mach_timebase_info_data_t  sTimebaseInfo;
  
 public:
  CStopWatch(){};
  void startTimer( );
  void stopTimer( );
  double getElapsedTime();
};

#endif

#endif // HR_TIME_H
