#include "hr_time.h"

#if __WIN32__

#include <windows.h>

#ifndef hr_timer
#include "hr_time.h"
#define hr_timer
#endif

double CStopWatch::LIToSecs( LARGE_INTEGER & L) {
	return ((double)L.QuadPart /(double)frequency.QuadPart);
}

CStopWatch::CStopWatch(){
	timer.start.QuadPart=0;
	timer.stop.QuadPart=0;	
	QueryPerformanceFrequency( &frequency );
}

void CStopWatch::startTimer( ) {
    QueryPerformanceCounter(&timer.start);
}

void CStopWatch::stopTimer( ) {
    QueryPerformanceCounter(&timer.stop);
}


double CStopWatch::getElapsedTime() {
	LARGE_INTEGER time;
	time.QuadPart = timer.stop.QuadPart - timer.start.QuadPart;
    return LIToSecs( time) ;
}

#endif // WIN32

#if __APPLE__

void CStopWatch::startTimer( ) {
  start = mach_absolute_time();
}

void CStopWatch::stopTimer( ) {
  end = mach_absolute_time();
}

double CStopWatch::getElapsedTime() {
  if ( sTimebaseInfo.denom == 0 ) {
    (void) mach_timebase_info( &sTimebaseInfo );
  }
  elapsed = end - start;
  return double(elapsed) * double(sTimebaseInfo.numer) / double(sTimebaseInfo.denom) / 1.0E09;
}

#endif  // APPLE && OSX_MAC
